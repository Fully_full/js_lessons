document.addEventListener('DOMContentLoaded', function() {
    
    let tabHeaderBlock  = document.querySelector('.info-header'),
        infoHeaderTab   = document.querySelectorAll('.info-header-tab'),
        infoTabContent  = document.querySelectorAll('.info-tabcontent')
    ;

    function hideTabContent(except) {
        for (let i = except; i < infoTabContent.length; i++)    {
            infoTabContent[i].classList.remove('show');
            infoTabContent[i].classList.add('hide');
        }
    }
    
    hideTabContent(1);


    function showTabContent(needed) {
        if (infoTabContent[needed].classList.contains('hide'))   {
            infoTabContent[needed].classList.remove('hide');
            infoTabContent[needed].classList.add('show');
        }
    }

    tabHeaderBlock.addEventListener('click', 
    function(event)  {
    
        let target = event.target;
            
        if ( target && target.classList.contains('info-header-tab') )
        {
            for(let i = 0; i < infoHeaderTab.length; i++) {
                if (target == infoHeaderTab[i]) {
                    hideTabContent(0);
                    showTabContent(i);
                    
                    break;
                }
            }
        }
    } );

});