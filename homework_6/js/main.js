'use strict';

let startButton      = document.getElementById("start"),                                // Получить кнопку "Начать расчет" через id
        // начало: Получить все блоки в правой части программы через классы
    budgetValue      = document.getElementsByClassName("budget-value")[0],
    dayBudget        = document.getElementsByClassName("daybudget-value")[0],
    incomeLevel      = document.getElementsByClassName("level-value")[0],
    expenses         = document.getElementsByClassName("expenses-value")[0],
    optionalExpenses = document.getElementsByClassName("optionalexpenses-value")[0],
    income           = document.getElementsByClassName("income-value")[0],
    monthsavings     = document.getElementsByClassName("monthsavings-value")[0],
    yearSavings      = document.getElementsByClassName("yearsavings-value")[0],
        // конец: Получить все блоки в правой части программы через классы
        
    inputExpenses    = document.getElementsByClassName("expenses-item"),                // expenses input -> array type!

        // start of getting buttons through @Tag
    confirmExpButton  = document.getElementsByTagName('button')[0],                      // confirm expenses button 
    confirmOptButton  = document.getElementsByTagName('button')[1],                      // confirm optional expenses button
    countBudget       = document.getElementsByTagName('button')[2],                      // calculating daily budget buuton                      // optional expenses
        //   end of getting buttons through @Tag

        // Начало: Получить поля для ввода необязательных расходов (optionalexpenses-item) при помощи querySelectorAll
    optExpensesitem   = document.querySelectorAll(".optionalexpenses-item"),
        // Конец: Получить поля для ввода необязательных расходов

        // Начало: Получить оставшиеся поля через querySelector
    chooseIncome     = document.querySelector('.choose-income'),
    savings          = document.querySelector('#savings'),
    sum              = document.querySelector('#sum'),
    percent          = document.querySelector('#percent'),
    yearValue        = document.querySelector('.year-value'),
    monthValue       = document.querySelector('.month-value'),
    dayValue         = document.querySelector('.day-value');
        // Конец: Получить оставшиеся поля


let money, time;

function start() {
    money = +prompt("Ваш бюджет на месяц?",'');
    time  = prompt('Введите дату в формате YYYY-MM-DD','');

    while( isNaN(money) || money == "" || money == null) {
        money = +prompt("Ваш бюджет на месяц?",'');
    }
}
start();


let appData = {
    budget: money,
    expenses: {},
    optionalExpences: {},
    income: [],
    timeDate: time,
    savings: true,

    chooseExpenses: function() {
        for (let i = 0; i < 2; i++) {
            let a = prompt("Введите обязательную статью расходов в этом месяце",''),
                b = prompt("Во сколько обойдётся?",'');
    
            if ( (typeof(a)) === 'string' && (typeof(a)) != null && (typeof(b)) != null
                && a != '' && b != '' && a.length < 50 ) {
                    console.log(a +" : " + b);
                    appData.expenses[a] = b;
            } else {
                // Задача: Вернуться на цикл обратно
                i--;
            }       
        }
    },
    detectDayBudget: function() {
        appData.moneyPerDay = (appData.budget/30).toFixed(2);
        alert("Ежедневный бюджет: " + appData.moneyPerDay);
    },
    detectLevel:function() {
        if ( appData.moneyPerDay < 100 ) {
            console.log("Минимальный уровень достатка");
        } else if ( appData.moneyPerDay > 100 && appData.moneyPerDay < 2000) {
            console.log("Средний уровень достатка");
        } else if ( appData.moneyPerDay > 2000) {
            console.log("Высокий уровень достатка");
        } else {
            console.log("Произошла ошибка");
        }
    },
    checkSavings: function() {
        if (appData.savings === true) {
            let save = +prompt("Какова сумма накоплений?",''),
                percent = +prompt("Под какой процент?",'');
    
                appData.monthIncome = (save/100/12*percent).toFixed(2);
                alert("Доход в месяц с вашего депозита: " + appData.monthIncome);
        }    
    },
    chooseOptExpenses: function() {
        for (let i=1; i < 4; i++) {
            let b = prompt("Статья необязательных расходов?", '');
         
            if (b != "" && typeof(b) != null && b.length < 50 )
            {
                // console.log(i + '. -> Расход: ' + b);
                appData.optionalExpences[i] = b;
            } else {
                i--;
            }
        }
    },

    chooseIncome: function() {
        let items = prompt("Что принесёт дополнительный доход? (Перечислите через запятую)",''),
            more = prompt('Может, что-то ещё? (Нажмите отмену, если нечего добавить)','');
        
        if (typeof(items) === "string" && items != null && items !="")
        {
            appData.income = items.split(',');
            appData.income.push(more);
            appData.income.sort();

            appData.income.forEach(
                function(item, number) {
                    console.log(number+1 + ': ' + item);
                })    
        } else {
            appData.chooseIncome();
        }
    },

    // Выводим ключи методов и свойств объекта appData
    info: function() {
        for (keys in appData) {
            console.log('Ключ: ' + keys);
        }
    }

}