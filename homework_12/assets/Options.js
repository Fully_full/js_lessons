class Options {
    constructor(height, width,  bg='bg.jpg', fontSize=19, textAlign='center') {
        this.height     = height;
        this.width      = width;
        this.bg         = bg;
        this.fontSize   = fontSize;
        this.textAlign  = textAlign;
    }
    
    createDiv() {
        let div     = document.createElement('div'),
            content = document.querySelector('.content');
    
            div.style.cssText = `
            font-size: ${this.fontSize}px;
            height: ${this.height}px; 
            width: ${this.width}px;
            background: url(img/${this.bg}) 100% 100% no-repeat;
            background-size: cover;
            text-align: ${this.textAlign};
            margin: 0 auto;
            `;
    
            div.innerText = 'Lorem Ipsum является текст-заполнитель макета.';
            content.appendChild(div);
    }
}
    
let options = new Options(520, 1200, 'bg.jpg', 26, 'center');
    options.createDiv();