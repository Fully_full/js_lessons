class OptionsMod {
    constructor(height, width, fontSize, textAlign='center', pathToImage='img', bg='bg.jpg', content = '') {
        this.height      = height;
        this.width       = width;
        this.bg          = bg;
        this.fontSize    = fontSize;
        this.textAlign   = textAlign;
        this.pathToImage = pathToImage;
        this.content     = content;
    }

    createDiv() {
        this.div    = document.createElement('div');
        let content = document.querySelector('.content');

        this.div.style.cssText = `
        font-size: ${this.fontSize}px;
        height: ${this.height}px; 
        width: ${this.width}px;
        background-size: cover;
        text-align: ${this.textAlign};
        `;

        this.isBgHex(this.bg);
        this.div.innerText = this.content;
        content.appendChild(this.div);
    }

    isBgHex (background) {
        let re = new RegExp("\#[A-Z0-9]{6}|\#[A-Z0-9]{3}");
            
        if(re.test(background)) {
            return this.div.style.cssText += `background-color: ${this.bg};`;
        } else {
            return this.div.style.cssText += `background: url(${this.pathToImage}/${this.bg}) 100% 100% no-repeat; background-attachment: scroll;`;
        }
    }
}

let content = 'Lorem Ipsum является текст-заполнитель обычно используется в графических, печать и издательской индустрии для предварительного просмотра макета и визуальных макетах.';

let options = new OptionsMod(900, 1200, 26, 'center', 'img', 'bg.jpg', content);
    options.createDiv();