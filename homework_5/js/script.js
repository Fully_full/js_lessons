'use strict';

let answer = prompt('Как вы относитель к технике Apple?', ''),
    place = document.getElementById('prompt'),
    newTitle = document.getElementById('title'),
    adv = document.querySelector('.adv'),
    column = document.querySelectorAll('.column'),
    menu = document.getElementsByClassName("menu")[0];

if (answer !== "" && answer !== null && answer.length < 130) {
    place.textContent = 'Ваше отношение к Apple, исходя из ответа: ' + answer;
} else {
    let altAnswer = 'Вы ничего не ответили про Ваше отношение к Apple :(';
    place.textContent = altAnswer;
}

document.body.style.backgroundImage = 'url("img/apple_true.jpg")';          // changing background
newTitle.textContent = 'Мы продаем только подлинную технику Apple';         // adding new word to title
column[1].removeChild(adv);                                                 // remove ad

let item1 = document.getElementsByClassName("menu-item")[1],
    item2 = document.getElementsByClassName("menu-item")[2];

    menu.insertBefore(item2, item1);                                        // swap items

let newItem = document.createElement('li');                                 // creating new element
    newItem.classList.add('menu-item');                                     // adding new property to created el.
    newItem.textContent = 'Пятый пункт';
    menu.appendChild(newItem);