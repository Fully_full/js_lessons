'use strict';

let money, time;


function start() {
    money = +prompt("Ваш бюджет на месяц?",'');
    time  = prompt('Введите дату в формате YYYY-MM-DD','');

    while( isNaN(money) || money == "" || money == null) {
        money = +prompt("Ваш бюджет на месяц?",'');
    }
}
start();


let appData = {
    budget: money,
    expenses: {},
    optionalExpences: {},
    income: [],
    timeDate: time,
    savings: true,

    chooseExpenses: function() {
        for (let i = 0; i < 2; i++) {
            let a = prompt("Введите обязательную статью расходов в этом месяце",''),
                b = prompt("Во сколько обойдётся?",'');
    
            if ( (typeof(a)) === 'string' && (typeof(a)) != null && (typeof(b)) != null
                && a != '' && b != '' && a.length < 50 ) {
                    console.log(a +" : " + b);
                    appData.expenses[a] = b;
            } else {
                // Задача: Вернуться на цикл обратно
                i--;
            }       
        }
    },
    detectDayBudget: function() {
        appData.moneyPerDay = (appData.budget/30).toFixed(2);
        alert("Ежедневный бюджет: " + appData.moneyPerDay);
    },
    detectLevel:function() {
        if ( appData.moneyPerDay < 100 ) {
            console.log("Минимальный уровень достатка");
        } else if ( appData.moneyPerDay > 100 && appData.moneyPerDay < 2000) {
            console.log("Средний уровень достатка");
        } else if ( appData.moneyPerDay > 2000) {
            console.log("Высокий уровень достатка");
        } else {
            console.log("Произошла ошибка");
        }
    },
    checkSavings: function() {
        if (appData.savings === true) {
            let save = +prompt("Какова сумма накоплений?",''),
                percent = +prompt("Под какой процент?",'');
    
                appData.monthIncome = (save/100/12*percent).toFixed(2);
                alert("Доход в месяц с вашего депозита: " + appData.monthIncome);
        }    
    },
    chooseOptExpenses: function() {
        for (let i=1; i < 4; i++) {
            let b = prompt("Статья необязательных расходов?", '');
         
            if (b != "" && typeof(b) != null && b.length < 50 )
            {
                // console.log(i + '. -> Расход: ' + b);
                appData.optionalExpences[i] = b;
            } else {
                i--;
            }
        }
    },

    chooseIncome: function() {
        let items = prompt("Что принесёт дополнительный доход? (Перечислите через запятую)",''),
            more = prompt('Может, что-то ещё? (Нажмите отмену, если нечего добавить)','');
        
        if (typeof(items) === "string" && items != null && items !="")
        {
            appData.income = items.split(',');
            appData.income.push(more);
            appData.income.sort();

            appData.income.forEach(
                function(item, number) {
                    console.log(number+1 + ': ' + item);
                })    
        } else {
            appData.chooseIncome();
        }
    },

    // Выводим ключи методов и свойств объекта appData
    info: function() {
        for (let keys in appData) {
            console.log('Ключ: ' + keys);
        }
    }

}